# SWR Bridge

Potential club project of a digital SWR bridge.  The idea is to put a
directional coupler on the PCB, and try to target it at relatively low
power VHF since that puts it in the interest range of many club
members. A PIC would read the voltages from the directional coupler, 
perform the calculations, and display the SWR.

It might also be reasonable to display power.

The initial PCB is simply a prototype platform. There are a number of
things to experiment with:

* How much voltage can we get from the simple coupler
* Do we need op amps, probably to provide gain but perhaps also impedance
* Do we need an op amp correction for the diode drop or can that be done in firmware
* How does the sensor line affect the impedance of the transmission strip
* How does the impedance of the sensor line affect the measured voltage
* Can we design for a reasonable range of power or do we need some sort of ranging capability
* Can a linearizing part such as the AD8307 log amplifier help

The specific PIC has not been selected.  Candidates are:

* PIC24FV16KM202
* PIC24FV32KA302
* dsPIC33EV32GM002

With some thought and creativity, we might be able to get away with
a PIC24FV32KA301.  Although physically smaller, the less common footprint
might result in a more expensive project.

Having decided to add a counter, the PIC choice is pretty  much limited to
the dsPIC33EVxxGMy02, with the dsPIC33EV32GM002 probably being the target.

